package com.ntfournier.breakout.simulation.actors;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import com.ntfournier.breakout.Constants;

public class Paddle extends BoundedGameActor {

    public Paddle(float x) {
        super(x, Constants.PADDLE_Y, Constants.PADDLE_WIDTH, Constants.PADDLE_HEIGHT);
    }

    public Vector2 getBounceDirection(float ballCenter) {
        float xDiff = ballCenter - this.getX(Align.center);
        return new Vector2(xDiff, 60).nor();
    }

}
