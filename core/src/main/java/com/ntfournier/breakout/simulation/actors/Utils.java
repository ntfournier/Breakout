package com.ntfournier.breakout.simulation.actors;

import com.badlogic.gdx.utils.Align;

public class Utils {
    public static float predictXCollision(Ball movingGameActor, GameActor staticGameActor) {
        if (movingGameActor.getY() > staticGameActor.getY(Align.top) && movingGameActor.getDirection().y < 0) {
            float distanceY = movingGameActor.getY() - staticGameActor.getY(Align.top);
            float timeToHit = distanceY / -movingGameActor.getDirection().y; // Speed is the same in X and Y.

            return movingGameActor.getX() + timeToHit * movingGameActor.getDirection().x;
        } else if (movingGameActor.getY(Align.top) < staticGameActor.getY() && movingGameActor.getDirection().y > 0) {
            float distanceY = staticGameActor.getY() - movingGameActor.getY(Align.top);
            float timeToHit = distanceY / movingGameActor.getDirection().y; // Speed is the same in X and Y.

            return movingGameActor.getX() + timeToHit * movingGameActor.getDirection().x;
        } else {
            return -1;
        }
    }
}
