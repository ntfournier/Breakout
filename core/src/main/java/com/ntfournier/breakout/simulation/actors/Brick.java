package com.ntfournier.breakout.simulation.actors;

import com.ntfournier.breakout.Constants;

public class Brick extends GameActor {

    public Brick(float x, float y) {
        super(x, y, Constants.BRICK_SIZE, Constants.BRICK_SIZE);
    }

    public void hit() {
        this.setHitPoints(this.getHitPoints() - 1);
    }
}
