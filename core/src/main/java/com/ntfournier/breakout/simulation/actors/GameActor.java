package com.ntfournier.breakout.simulation.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;
import com.ntfournier.breakout.Constants;
import com.ntfournier.breakout.simulation.EventHandler;

public class GameActor extends Actor {
    public EventHandler eventHandler;
    private float cornerSize = Constants.ACTOR_DIAGONAL_COLLISION_SIZE;
    private int hitPoints = 1;
    private float stateTime;

    public GameActor(float x, float y, float width, float height) {
        this.setX(x);
        this.setY(y);
        this.setWidth(width);
        this.setHeight(height);
        this.setBounds(x, y, width, height);

        this.eventHandler = new EventHandler();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        this.stateTime += delta;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        // skip
    }

    public int getNineAreaCollision(GameActor gameActor) {
        if (this.getX() < gameActor.getX(Align.right) &&
                this.getX(Align.right) > gameActor.getX() &&
                this.getY() < gameActor.getY(Align.top) &&
                this.getY(Align.top) > gameActor.getY()) {
            int horizontalAlign = 0;
            if (gameActor.getX() > this.getX(Align.right) - cornerSize) {
                horizontalAlign = Align.right;
            } else if (gameActor.getX(Align.right) < this.getX() + cornerSize) {
                horizontalAlign = Align.left;
            }

            int verticalAlign = 0;
            if (gameActor.getY() > this.getY(Align.top) - cornerSize) {
                verticalAlign = Align.top;
            } else if (gameActor.getY(Align.top) < this.getY() + cornerSize) {
                verticalAlign = Align.bottom;
            }

            return verticalAlign | horizontalAlign;
        } else {
            return 0;
        }
    }

    public boolean overlaps(GameActor gameActor) {
        return new Rectangle(this.getX(), this.getY(), this.getWidth(), this.getHeight())
                .overlaps(new Rectangle(gameActor.getX(),
                                        gameActor.getY(),
                                        gameActor.getWidth(),
                                        gameActor.getHeight()));
    }

    public String getLogString() {
        return String.format("%-15s x: %8.2f  y: %8.2f  w: %8.2f  h: %8.2f",
                             this.getClass().getSimpleName(),
                             this.getX(), this.getY(),
                             this.getWidth(), this.getHeight());
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public void setHitPoints(int hitPoints) {
        this.hitPoints = hitPoints;
    }

    public float getStateTime() {
        return this.stateTime;
    }
}
