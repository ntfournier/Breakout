package com.ntfournier.breakout;

import com.badlogic.gdx.graphics.Color;
import com.ntfournier.breakout.utils.RGBColor;

import java.util.*;

public class Constants {
    public static final String SKIN_LOCATION = "gdx-skins-master/clean-crispy/skin/clean-crispy-ui.json";
    public static final int SIMULATION_WIDTH = 720;
    public static final int SIMULATION_HEIGHT = 1280;

    public static final int ACTOR_DIAGONAL_COLLISION_SIZE = 12;

    public static final int BRICK_SIZE = 28;

    public static final int NB_BRICK_COLUMNS = 14;
    public static final int NB_BRICK_ROWS = 5;
    public static final int SOLID_BRICK_DEFAULT_HIT_POINTS = 3;
    public static final float BRICK_POWER_UP_YIELD = .2f;

    public static final int PADDLE_Y = 128;

    public static final int PADDLE_WIDTH = 128;
    public static final int PADDLE_HEIGHT = 16;

    public static final float BALL_DIRECTION_RANDOM = .2f;
    public static final int BALL_SIZE = 16;
    public static final float BALL_SPEED = 500;

    public static final String EVENT_BALL_HIT_BOTTOM = "EVENT_BALL_HIT_BOTTOM";
    public static final String EVENT_NO_BALL_LEFT = "EVENT_NO_BALL_LEFT";
    public static final String EVENT_BRICK_DESTROYED = "EVENT_BRICK_DESTROYED";
    public static final String EVENT_NO_BRICK_LEFT = "EVENT_NO_BRICK_LEFT";
    public static final String EVENT_PADDLE_HIT = "EVENT_PADDLE_HIT";
    public static final String EVENT_BALL_CREATED = "EVENT_BALL_CREATED";
    public static final String EVENT_POWER_UP_COLLECTED = "EVENT_POWER_UP_COLLECTED";

    public static final Map<String, Map<Integer, Color>> PALETTES;
    public static final float POWER_UP_WIDTH = 38;
    public static final float POWER_UP_HEIGHT = 16;
    public static final float POWER_UP_SPEED = 230;
    public static final String EVENT_BRICK_CREATED = "EVENT_BRICK_CREATED";
    public static final String EVENT_POWER_UP_CREATED = "EVENT_POWER_UP_CREATED";
    public static final float POWER_UP_PADDLE_SHRINK_FACTOR = .70f;
    public static final float POWER_UP_PADDLE_EXPAND_FACTOR = 1.40f;

    static {
        Map<String, Map<Integer, Color>> p = new HashMap<>();

        Map<Integer, Color> glowAndShine = new HashMap<>();
        glowAndShine.put(1, new RGBColor(38, 37, 46));

        glowAndShine.put(2, new RGBColor(66, 72, 93));
        glowAndShine.put(3, new RGBColor(104, 119, 147));
        glowAndShine.put(4, new RGBColor(171, 183, 208));

        glowAndShine.put(5, new RGBColor(147, 224, 168));
        glowAndShine.put(6, new RGBColor(116, 181, 177));
        glowAndShine.put(7, new RGBColor(248, 144, 171));

        p.put("glow_and_shine", glowAndShine);

        Map<Integer, Color> neonRave = new HashMap<>();

        neonRave.put(1, new RGBColor(147, 224, 168));
//        neonRave.put(2, new Color())

        PALETTES = Collections.unmodifiableMap(p);
    }
}
