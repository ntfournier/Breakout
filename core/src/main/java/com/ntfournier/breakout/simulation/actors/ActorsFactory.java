package com.ntfournier.breakout.simulation.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.utils.Align;
import com.ntfournier.breakout.Constants;
import com.ntfournier.breakout.simulation.Simulation;
import com.ntfournier.breakout.simulation.actors.powerups.*;

import static com.ntfournier.breakout.Constants.BALL_SIZE;

public class ActorsFactory {
    private final int simulationHeight;
    private final int simulationWidth;
    public Simulation simulation;

    public ActorsFactory(Simulation simulation, int simulationWidth, int simulationHeight) {
        this.simulation = simulation;
        this.simulationWidth = simulationWidth;
        this.simulationHeight = simulationHeight;
    }

    public Paddle createPaddle() {
        Paddle paddle = new Paddle(this.simulationWidth / 2);
        paddle.setOuterBounds(new Rectangle(0, 0, this.simulationWidth, this.simulationHeight));
        return paddle;
    }

    public Ball createBallOnPaddle() {
        return this.createBall(this.simulation.getPaddle().getX(Align.center),
                               this.simulation.getPaddle().getY(Align.top),
                               new Vector2(1, 1));
    }

    public Ball createBall(float x, float y, Vector2 direction) {
        Ball ball = new Ball(x, y, direction);
        ball.setOuterBounds(new Rectangle(0,
                                          -2 * BALL_SIZE,
                                          Constants.SIMULATION_WIDTH,
                                          Constants.SIMULATION_HEIGHT + Constants.BALL_SIZE * 2));
        return ball;
    }

    public PowerUp createRandomPowerUp(float x, float y) {
        int powerNb = (int) (Math.random() * 3);

        PowerUp powerUp;
        switch (powerNb) {
            case 0:
                powerUp = new MultiplyBalls(x, y);
                powerUp.setColor(Color.GREEN);
                break;
            case 1:
                powerUp = new ChangePaddleWidth(x, y, Constants.POWER_UP_PADDLE_SHRINK_FACTOR);
                powerUp.setColor(Color.RED);
                break;
            case 2:
                powerUp = new ChangePaddleWidth(x, y, Constants.POWER_UP_PADDLE_EXPAND_FACTOR);
                powerUp.setColor(Color.GREEN);
                break;
            default:
                System.out.println("Oups");
                powerUp = new MultiplyBalls(x, y);
        }

        return powerUp;
    }
}
